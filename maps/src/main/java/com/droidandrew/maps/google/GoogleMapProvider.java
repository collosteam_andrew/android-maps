package com.droidandrew.maps.google;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.IdRes;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.droidandrew.maps.core.BaseMapProvider;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by andrew on 26.10.16.
 */

public class GoogleMapProvider extends BaseMapProvider<GoogleMap> implements OnMapReadyCallback {

    private GoogleMap googleMap;

    public GoogleMapProvider(Activity activity, @IdRes int containerId) {
        super(activity, containerId);
    }

    @Override
    public void draw(@IdRes int container) {
        FrameLayout fl = getActivity().findViewById(container);
        fl.removeAllViews();
        MapFragment mapFragment = MapFragment.newInstance();
        getActivity().getFragmentManager().beginTransaction().replace(container, mapFragment, "MAP").commitAllowingStateLoss();
        mapFragment.getMapAsync(this);
    }

    @Override
    public void destroy() {
        googleMap.clear();
        googleMap = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        Toast.makeText(getActivity(), "Map Ready", Toast.LENGTH_SHORT).show();
        googleMap.setMyLocationEnabled(true);
        googleMap.addPolyline(new PolylineOptions().add(new LatLng(0, 0)).add(new LatLng(65, 55)).color(Color.YELLOW));
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(new LatLng(65, 55)).zoom(3).bearing(2).build()));
    }
}
