package com.droidandrew.maps.yandex;

import android.app.Activity;
import android.app.Fragment;
import android.support.annotation.IdRes;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.droidandrew.maps.R;
import com.droidandrew.maps.core.BaseMapProvider;

import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.map.MapEvent;
import ru.yandex.yandexmapkit.map.OnMapListener;

/**
 * Created by andrew on 26.10.16.
 */

public class YandexMapProvider extends BaseMapProvider<MapView> implements OnMapListener {

    public YandexMapProvider(Activity activity, @IdRes int containerId) {
        super(activity, containerId);
    }

    @Override
    public void draw(@IdRes int container) {
        FrameLayout fl = getActivity().findViewById(container);
        fl.removeAllViews();
        Fragment fragment = getActivity().getFragmentManager().findFragmentByTag("MAP");
        if (fragment != null) {
            getActivity().getFragmentManager().beginTransaction().remove(fragment).commit();
        }
        MapView map = new MapView(fl.getContext(), fl.getContext().getString(R.string.yandex_map_app_id));
        fl.addView(map, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        onMapReady(map);
    }

    @Override
    public void onMapReady(MapView map) {
        map.showFindMeButton(true);
        map.showJamsButton(false);
        map.showZoomButtons(false);
    }

    @Override
    public void destroy() {

    }

    @Override
    public void onMapActionEvent(MapEvent mapEvent) {
        Toast.makeText(getActivity(), "Map Event " + mapEvent.getMsg(), Toast.LENGTH_SHORT).show();
    }
}
