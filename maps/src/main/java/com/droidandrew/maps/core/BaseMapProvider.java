package com.droidandrew.maps.core;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

/**
 * Created by andrew on 26.10.16.
 */

public abstract class BaseMapProvider<M> {

    private final WeakReference<Activity> weakReference;
    @IdRes
    private int containerId;

    public BaseMapProvider(Activity activity, @IdRes int containerId) {
        weakReference = new WeakReference<>(activity);
        this.containerId = containerId;
    }

    @Nullable
    public Activity getActivity() {
        return weakReference.get();
    }

    @IdRes
    public int getContainerId() {
        return containerId;
    }

    public abstract void draw(@IdRes int container);

    public abstract void onMapReady(M map);

    public void apply() {
        draw(getContainerId());
    }


    public abstract void destroy();
}
