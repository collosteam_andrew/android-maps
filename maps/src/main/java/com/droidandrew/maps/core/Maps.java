package com.droidandrew.maps.core;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.annotation.IntDef;
import android.util.SparseArray;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by andrew on 26.10.16.
 */

public class Maps {
    public static final int NONE = -1;
    public static final int GOOGLE = 657;
    public static final int YANDEX = 658;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({NONE, GOOGLE, YANDEX})
    public @interface Provider {
    }

    @Provider
    private int currentProvider = NONE;
    private SparseArray<BaseMapProvider> mapsCache = new SparseArray<>();

    private Activity activity;
    @IdRes
    private final int containerId;

    public Maps(Activity activity, int containerId) {
        this.activity = activity;
        this.containerId = containerId;
    }

    /**
     * Check current state of controller and apply provider from method parameters all skip if current provider equals to new.
     *
     * @param provider new provider for show
     * @return true if provider changed or false if not
     */
    public boolean apply(@Provider int provider) {
        if (currentProvider != provider) {
            currentProvider = provider;
            BaseMapProvider mapProvider = mapsCache.get(provider);
            if (mapProvider == null) {
                mapProvider = MapsFactory.create(activity, containerId, provider);
                mapsCache.put(provider, mapProvider);
                mapProvider.apply();
                return true;
            } else {
                mapProvider.apply();
            }
        }
        return false;
    }

    public void destroy() {
        int size = mapsCache.size();
        for (int i = 0; i < size; i++) {
            BaseMapProvider mapProvider = mapsCache.valueAt(i);
            mapProvider.destroy();
        }
        activity = null;
    }

}
