package com.droidandrew.maps.core;

import android.app.Activity;
import android.support.annotation.IdRes;

import com.droidandrew.maps.google.GoogleMapProvider;
import com.droidandrew.maps.yandex.YandexMapProvider;

/**
 * Created by andrew on 26.10.16.
 */

public class MapsFactory {

    public static BaseMapProvider create(Activity activity, @IdRes int container, @Maps.Provider int provider) {
        switch (provider) {
            case Maps.GOOGLE:
                return new GoogleMapProvider(activity, container);
            case Maps.YANDEX:
                return new YandexMapProvider(activity, container);
            case Maps.NONE:
            default:
                return null;
        }
    }

}
