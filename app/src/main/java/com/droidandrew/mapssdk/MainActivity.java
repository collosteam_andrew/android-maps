package com.droidandrew.mapssdk;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.droidandrew.maps.core.Maps;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    private static final String TAG = "MainActivity_";

    @BindView(R.id.container)
    ViewGroup mContainer;

    @BindView(R.id.rbgChoose)
    RadioGroup mRbgChoose;

    private Maps maps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mRbgChoose.setOnCheckedChangeListener(this);
        maps = new Maps(this, R.id.container);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRbgChoose.setOnCheckedChangeListener(null);
        maps.destroy();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rbLeft:
                Log.d(TAG, "onCheckedChanged: Google");
                maps.apply(Maps.GOOGLE);
                break;

            case R.id.rbRight:
                maps.apply(Maps.YANDEX);
                Log.d(TAG, "onCheckedChanged: Yandex");
                break;
        }
    }
}
